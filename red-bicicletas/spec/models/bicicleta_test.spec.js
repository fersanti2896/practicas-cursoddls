var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function() {
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeAll(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useCreateIndex', true);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });
    
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia en Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "Azul", "Benoto", [-34.2, 23.324]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe("Azul");
            expect(bici.modelo).toBe("Benoto");
            expect(bici.ubicacion[0]).toBe(-34.2);
            expect(bici.ubicacion[1]).toBe(23.324);
        });
    }); 

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            Bicicleta.add(aBici, function(err, newBici) {
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Debe devolver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 2, color: "Azul", modelo: "Chabelo"});
                Bicicleta.add(aBici, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 3, color: "Rojo", modelo: "Benoto"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);

                        Bicicleta.findByCode(2, function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

});


/* Parte de test con datos en memoria
// Para limpiar la colección de nuestras bicis y que no haya problema repitiendo el codigo
beforeEach(() => {Bicicleta.allBicis = []; });

describe('Bicicleta.allBicis', () => {
    it('Comienza vacío', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'Roja', 'Urbana', [19.2808717,-99.0559438]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('Debe devolver el id de la bici 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, "Verde", "Benoto");
        var aBici2 = new Bicicleta(2, "Azul", "Chabelo");

        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});

describe('Bicicleta.removeById', () => {
    it('Debe eliminar la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0); 
        
        var bici1 = new Bicicleta(1, "Roja", "Optimus");
        Bicicleta.add(bici1);
        Bicicleta.removeById(1);

        expect(Bicicleta.allBicis.length).toBe(0);
    });
});
*/