var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');

// Se incorpora el server en 'bin/www' de manera que no se tenga que levantar el servidor manualmente
// y se trae el módulo que exporta bin/www
var server = require('../../bin/www');

var base_url = 'http://localhost:5000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeAll((done) => { mongoose.connection.close(done) });

    beforeAll(function(done) {
        var mongoDB = 'mongodb://localhost:27017/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });
    });
    
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('GET Bicicletas /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id": 10, "color": "azul", "modelo": "Chabelo", "lat": -23, "lng": -34}';
            request.post({
                headers: headers, 
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                console.log(response.statusCode);
                console.log(response.statusMessage);
                console.log(JSON.parse(body));

                var bici = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(bici.color).toBe('azul');
                expect(bici.ubicacion[0]).toBe(-23);
                expect(bici.ubicacion[1]).toBe(-34);
                done();
            });
        });
    });
}); 

/*
describe('Bicicleta API', () => {
    describe('GET Bicicletas /', () => {
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'Roja', 'Urbana', [19.2808717,-99.0559438]);
            Bicicleta.add(a);

            // Se instala el request con 'npm install request -save'
            request.get('http://localhost:5000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST Bicicletas /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id" : 10, "color": "Azul", "modelo": "Chabelo", "lat": -23, "lng": -34}';
            request.post({
                headers: headers, 
                url: 'http://localhost:5000/api/bicicletas/create',
                body: aBici
            },
            function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("Azul");
                done();
            });
        });
    });
});
*/