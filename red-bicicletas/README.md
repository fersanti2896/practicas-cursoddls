# Proyecto. Red-Bicicletas

#### RESUMEN MÓDULO 1
Este es el proyecto _red-bicicletas_ creado en este repositorio correspondiente al ___módulo 1___.

Se detalla todos los elementos que componen a este proyecto, como la implementanción de un mapa donde se ubica a bicicletas y el uso de una API funcionando con Postman. 

#### DESARROLLO MÓDULO 1

__1. Mensaje de bienvenidad a Express__

Para ejecutar nuestro servidor hacemos:

    npm run devstart

El resultado es el siguiente:

![punto1](public/images/mensaje-express.png)

__2. Proyecto vinculado con el repositorio de Bitbucket__

Para este punto se ejecuta en la terminal de Git Bash:

    git remote -v

Que nos duvelve la ruta que vincula el proyecto local con el proyecto alojado en _Bitbucket_ en dicho proyecto se aloja nuestro repositorio.

![punto2](public/images/ruta-repositorio.PNG)

__3. El servidor actual con el anterior.__

El servidor se ejecuta con el comando:

    npm run devstart

Que nos devuelve el estatus del servidor en la terminal: 

![punto3](public/images/servidorActual.PNG)

__4. Mapa centrado en una ciudad.__

Se usa una etiqueta en nuestro archivo pug llamada _#main_map_ y en nuestro archivo estilo agregamos dicha etiqueta con las dimensiones. Nuestro mapa se ve de la siguiente forma, sin agregar ubicaciones de bicicletas. 

![punto4](public/images/mapaCiudad.PNG)

__5. Marcadores centrado en una ciudad.__

En este punto se manejan dos ubicaciones dentro del mapa, mismas que al iniciar el servidor se cargan dichas ubicaciones, pues se almacenan en memoria. 

![punto5](public/images/mapaUbicacion.PNG)

__6. Un script npm en el archivo package.json debajo del 'start' un 'devstart' y que levanta el servidor.__

En el punto 3 se tiene una imagen donde se levanta el servidor con el comando

    npm run devstart

En la siguiente imagen se muestra el script donde se levanta el archivo del servidor. 

![punto6](public/images/scriptServidor.PNG)

__7. Un par de bicicletas en la colección de bicicletas en memoria__

En el punto 5 se muestra el mapa con dos ubicaciones de bicicletas, pero en nuestro modelo guardamos/agregamos dos bicicletas a la colección de bicicletas, para que cada vez que se carge/reinicie el servidor, estas se cargen por defecto y se muestren en el mapa. 

![punto7](public/images/coleccionBicicletas.PNG)

__8. Una colección del modelo de bicicleta.__

En el punto anterior se guardan dos bicicletas en memoria, es decir, en la colección de las bicicletas, pero al momento de mostrar estas bicicletas en una tabla se hace con un método que se describe a continuación:

    Bicicleta.allBicis = [];
    Bicicleta.add = function(aBici) {
        Bicicleta.allBicis.push(aBici);
    }

Así, se muestra en la siguiente imagen, todas las bicicletas en una lista. 

![punto8](public/images/listaBicicletas.PNG)

__9. Endpoints de la API con Postman.__

Para este punto se muestran tres tipos de endpoints: 
 
- __Listar todas las bicicletas.__
    Se muestra a continuación la URL donde se hará la consulta: 
    
        localhost:3000/api/bicicletas

    Se tiene como resultado, la colección de bicicletas: 

    ![punto9a](public/images/postmanLista.PNG)        

 - __Crear una bicicleta.__ 
    Se muestra a continuación la URL donde se hará la consulta, donde debe los 4 parámetros que son el ID, Color, Modelo y la Ubicación con el método POST:

        localhost:3000/api/bicicletas/create
    
    Se tiene como resultado al agregar una nueva bicicleta: 

    ![punto9b](public/images/postmanCrear.PNG)

    Visto desde el mapa dicha bicicleta con ID: 4

    ![punto9b2](public/images/nuevaBici.PNG)

 - __Eliminar una bicicleta.__ 
    Se muestra a continuación la URL donde se eliminará una bicicleta, pasándole como parámetro el ID de la bicicleta con el método DELETE: 

        localhost:3000/api/bicicletas/delete
    
    Se muestra el resultado al eliminar la bicicleta con el ID: 2

    ![punto9c](public/images/postmanEliminar.PNG)

    Visto desde el mapa, donde solo nos quedamos con la primera bicicleta guardada en memoria y la que creamos en el punto anterior. 

    ![punto9c2](public/images/eliminacionBici.PNG)

#### CONCLUSIONES MÓDULO 1
Como se puede observar, se ha llegado al objetivo del módulo, donde vimos cómo crear un proyecto, subirlo a _Bitbucket_, crear nuestro servidor e ir añadiendo los modelos, vistas y controladores a nuestro proyecto, así como implementar su CRUD de nuestro proyecto. 

#### RESUMEN MÓDULO 2
Este es el proyecto _red-bicicletas_ creado en este repositorio correspondiente al ___módulo 2___.

Se detalla todos los elementos que componen a este proyecto, como la implementanción de la base de datos _MongoDB_ y el uso de testing. 

#### DESARROLLO MÓDULO 2

__1. Una carpeta con nombre 'models' dentro de la carpeta spec__

Como podemos observar, dentro de nuestro directorio tenemos una carpeta dentro de _spec_ la carpeta _models_ con sus respectivos testing de bicicleta y usuario. 

![punto2-1](public/images/models_spec.PNG)

__2. Los tests aprobados__

En correlación a la pregunta anterior, al ejecutar cada test que está dentro de la carpeta _models_ tenemos primero para la _bicicleta_test_:

![punto2-2-1](public/images/bicicleta_test.PNG)

Para el test de usuarios, es decir, del archivo _usuario_test_ tenemos: 

![punto2-2-2](public/images/usuario_test.PNG)

__3. Tests de cada operación de la API de bicicleta__

Ejecutando el comando: 

    jasmine spec/api/bicicleta_api_test.spec.js

Tenemos que nuestro test nos devuelve: 

![punto2-3](public/images/bicicleta_api_test.PNG)

__4. Un archivo bicicleta_api_test.spec.js__

Dentro de nuestro proyecto tenemos ubicado a nuestro archivo _bicicleta_api_test_:

![punto2-4](public/images/directorio_bicicleta_api.PNG)

__5. Todos los tests aprobados al ejecutar el comando npm test__

Al ejecutar en la terminal el comando

    npm run test

Nos ejecutará todos nuestros test de manera general, la cual obtenemos sin errores: 

![puntp2-5](public/images/run_test.PNG)

__6. Las colecciones desde tu base local de Mongo__

Podemos ver através desde nuestro IDE de MongoBD, que por el momento no tenemos colecciones o documentos, pues aún no hemos guardado registros. 

![punto2-6](public/images/sin_collection.PNG)

__7. Una base local mongo llamada "red_bicicletas"__

Para este ejemplo manejamos una base que se llama _red_bicicletas_ que guarda nuestras colecciones.

![punto2-7](public/images/base_red_bicicletas.PNG)

__8. El modelo funcional utilizando Postman__

Usando Postman, probamos las funciones de listar y crear. 

Para listar usamos la URL:

    localhost:5000/api/bicicletas

Donde notamos que tenemos una lista vacía: 

![punto2-8-1](public/images/api_bicicletas.PNG)

Ahora creamos un nuevo elemento de bicicleta, por lo que pasamos como párametros:

    localhost:5000/api/bicicletas/create

    {
        "color": "Verde",
        "modelo": "Benoto",
        "lat": "-23.323",
        "lng": "32.345"
    }

Y obtenemos como respuesta: 

![punto2-8-2](public/images/creandoBici.PNG)

Creamos un usuario ya que nuestra lista se encuentra vacía, para ello hacemos: 

    localhost:5000/api/usuarios/create

    {
        "nombre": "Fernando Nicolás"
    }

Y obtenemos como respuesta: 

![punto2-8-3](public/images/creandoUsuario.PNG)

Ahora reservamos un usuario con una bicicleta, para ello vamos con la dirreción:

    localhost:5000/api/usuarios/reservar

    {
        "id": "5f72d028a025d01b48341bc5",
        "bici_id": "5f72cf55a025d01b48341bc4",
        "desde": "2020-09-27",
        "hasta": "2020-10-15"
    }

Y obtenemos como respuesta un 200 OK, como lo muestra la imagen: 

![punto2-8-4](public/images/reservandoUser.PNG)

__9. Documentos generados en la base local__

Verificamos dentro de nuestro IDE Robo 3T, haciendo consultas a nuestros documentos tenemos que para el documento _Usuarios_:

![punto2-9-1](public/images/usuariosDoc.PNG)

Para el documento _Reservas_:

![punto2-9-2](public/images/reservandoDoc.PNG)

Para el documento _Bicicletas_: 
![punto2-9-3](public/images/bicicletaDoc.PNG)


__10. Tests del modelo Bicicleta que usan persistencia__

Tiene que ver con el punto 2 donde vemos los test de la carpeta _models_.

#### CONCLUSIONES MÓDULO 2
Como se puede observar, se ha llegado al objetivo del módulo, que es hacer los testing a nuestros modelos y nuestra API, además de incluir nuestra base de datos no relacional, en este caso MongoBD.
